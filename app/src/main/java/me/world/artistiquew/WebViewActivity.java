package me.world.artistiquew;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import me.world.artistiquew.databinding.ActivityWebViewBinding;

public class WebViewActivity extends AppCompatActivity {
    private long backPressed;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActivityWebViewBinding webViewBinding = DataBindingUtil.setContentView(WebViewActivity.this, R.layout.activity_web_view);
        webViewBinding.webView.loadUrl("https://artistiquew.com/");

        webViewBinding.webView.getSettings().setJavaScriptEnabled(true);
        webViewBinding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webViewBinding.webView.getSettings().setDomStorageEnabled(true);
        webViewBinding.webView.addJavascriptInterface(new Object(){
            @JavascriptInterface
            public void performClick()
            {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK,returnIntent);
                WebViewActivity.this.finish();
            }
        },"close");

        webViewBinding.webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }
        });
    }
    @Override
    public void onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        }else {
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_LONG).show();
            backPressed = System.currentTimeMillis();
        }
    }
}