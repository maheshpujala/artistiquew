package me.world.artistiquew;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import me.world.artistiquew.databinding.ActivitySplashBinding;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {
    private long backPressed;
    ActivitySplashBinding activitySplashBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        Animation animFadein = AnimationUtils.loadAnimation(this,
                R.anim.fade_in);
        activitySplashBinding.appLogo.setAnimation(animFadein);

        new Handler().postDelayed(() -> {
            Intent webView = new Intent(SplashActivity.this, WebViewActivity.class);
            startActivity(webView);
            this.finish();
        }, 3000);
    }

    @Override
    public void onBackPressed() {
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        }else {
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_LONG).show();
            backPressed = System.currentTimeMillis();
        }
    }
}